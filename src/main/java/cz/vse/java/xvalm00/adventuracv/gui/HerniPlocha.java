package cz.vse.java.xvalm00.adventuracv.gui;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;

public class HerniPlocha {

    private AnchorPane anchorPane = new AnchorPane();

    private Circle tecka = new Circle(10.0, Paint.valueOf("RED"));

    private ImageView imageView;

    public HerniPlocha() {
        Image image = new Image(HerniPlocha.class.getClassLoader().getResourceAsStream("herniPlan.png"), 400, 250,
                false, false);
        imageView = new ImageView(image);

        anchorPane.getChildren().addAll(imageView, tecka);
        AnchorPane.setLeftAnchor(tecka, 145.0);
        AnchorPane.setTopAnchor(tecka, 80.0);
    }

    public AnchorPane getAnchorPane() {
        return anchorPane;
    }
}